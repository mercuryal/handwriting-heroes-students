export const MyFunctions = {
  UnityProgress: function(unityInstance, progress) {
    console.log("Unity progress module called");  
    if (!unityInstance.Module)
      return;
    if (!unityInstance.logo) {
      console.log("No unity instance");
      unityInstance.logo = document.createElement("div");
      unityInstance.logo.className = "logo " + unityInstance.Module.splashScreenStyle;
      unityInstance.container.appendChild(unityInstance.logo);
    }
    if (!unityInstance.progress) {
      console.log("No unity progress");    
      unityInstance.progress = document.createElement("div");
      unityInstance.progress.className = "progress " + unityInstance.Module.splashScreenStyle;
      unityInstance.progress.empty = document.createElement("div");
      unityInstance.progress.empty.className = "empty";
      unityInstance.progress.appendChild(unityInstance.progress.empty);
      unityInstance.progress.full = document.createElement("div");
      unityInstance.progress.full.className = "full";
      unityInstance.progress.appendChild(unityInstance.progress.full);
      unityInstance.container.appendChild(unityInstance.progress);
    }
    unityInstance.progress.full.style.width = (100 * progress) + "%";
    unityInstance.progress.empty.style.width = (100 * (1 - progress)) + "%";
    if (progress == 1)
      unityInstance.logo.style.display = unityInstance.progress.style.display = "none";
  }
};
