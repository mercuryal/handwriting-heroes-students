import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '../views/Login.vue'
import Game from '../views/Game.vue'
import PasswordReset from '../views/PasswordReset.vue'

import * as firebase from "firebase/app";
import "firebase/auth";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Game,
    meta: {requiresAuth: true}
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/game/:uid',
    name:'game',
    component: Game,
    meta: {requiresAuth: true},
    props: true
  },
  {
    path: '/passwordReset',
    name: 'passwordReset',
    component: PasswordReset
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const isAuthenticated = firebase.default.app("secondaryFirebaseApp").auth().currentUser;

  if(requiresAuth && !isAuthenticated){
    next("/login");
  }
  else{
    next();
  }
})

export default router
