import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios"
import firebase from "firebase/app"
import Unity from 'vue-unity-webgl'
import Toasted from 'vue-toasted'
import Vuelidate from 'vuelidate'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import './quasar/quasar.js'

Vue.prototype.$axios = axios;
library.add(faUserSecret)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

Vue.use(Toasted);
Vue.use(Vuelidate);

//firebase initialization
var firebaseConfig = {
  apiKey: "AIzaSyBIQEGc0KXmSLE5KZfhUnDmI6t-SmmAC9M",
  authDomain: "handwriting-heroes.firebaseapp.com",
  databaseURL: "https://handwriting-heroes.firebaseio.com",
  projectId: "handwriting-heroes",
  storageBucket: "handwriting-heroes.appspot.com",
  messagingSenderId: "581167089789",
  appId: "1:581167089789:web:7ea03669f6e1d0120fc914",
  measurementId: "G-LSF1PRHZ9R"
};
var secondaryFirebaseApp = firebase.initializeApp(firebaseConfig, "secondaryFirebaseApp");
let app;

secondaryFirebaseApp.auth().onAuthStateChanged(user => {
  console.log(user);
  if(!app){
    app = new Vue({
      router,
      store,
      data () {
        return {
          loggedIn: false,
          user: undefined,
          student: {
            email: "",
            name: "",
            lastName: "",
            teacherId: "",
            uid: "",
          },
          teacher: "",
          userType: "",
          displayName: "",
          userPlan: 10,
          error: "",
          signingIn: false,
          plansId: {
            10: "noPlan",
            8412: "demo",
            8414: "individual",
            8450: "class",
            8451: "school",
          },
          settings: {
            bouncers: true,
            cannonPops: true,
            skiers: true,
            skydivers: true,
            surfers: true,
            userHand: 0,
            userProgressionMode: 0,
          }
        };
      },
      computed: {
        isLoggedIn () {
          return this.loggedIn;
        },
      },
      mounted () {
        var token = this.getParameterByName('token');
     
        if ( token ) { // Hanlde login coming from Clever
          secondaryFirebaseApp.auth().signOut();
          secondaryFirebaseApp
          .auth()
          .signInWithCustomToken(token)
          .then((result) => {
            secondaryFirebaseApp.database().ref("/teachers/" + result.user.uid + "/email/").set(result.user.email);
            var user = result.user;
        
            if ( user && !this.signingIn) {
              if ( user.displayName )
                this.$root.displayName = user.displayName;
              else
                this.$root.displayName = user.email;
                
              var dbRef = firebase.default.app("secondaryFirebaseApp").database()
                .ref("/students/" + user.uid + "/teacherId");
                dbRef.once("value").then((snapshot) => {
                  firebase.default.app("secondaryFirebaseApp").database().ref("/teachers/" + snapshot.val() + "/email")
                  .once("value").then((childSnapshot) => {
                    axios.get('https://handwritingheroes.org/hh-online/config/userinfo.php', { params: { action: "get", userid: childSnapshot.val(), } })
                    .then((response)=> {
                      this.userPlan = response.data["plan_id"];
                      console.log(this.userPlan);
                    })
                  })
                });
                this.setUserLogged(user);
            }
          })

        } else {
          firebase.default.app("secondaryFirebaseApp").auth().onAuthStateChanged((user) => {
            if (user && !this.signingIn) {
              var dbRef = firebase.default.app("secondaryFirebaseApp").database()
              .ref("/students/" + user.uid + "/teacherId");
              dbRef.once("value").then((snapshot) => {
                firebase.default.app("secondaryFirebaseApp").database().ref("/teachers/" + snapshot.val() + "/email")
                .once("value").then((childSnapshot) => {
                  axios.get('https://handwritingheroes.org/hh-online/config/userinfo.php', { params: { action: "get", userid: childSnapshot.val(), } })
                  .then((response)=> {
                    this.userPlan = response.data["plan_id"];
                    console.log(this.userPlan);
                  })
                })
              });
              this.setUserLogged(user);
            } else {
              this.loggedIn = false;
              this.user = null;
            }
          });
        }
      },
      methods: {
        getParameterByName: function(name, url = window.location.href){
          name = name.replace(/[[\]]/g, '\\$&');
          var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
              results = regex.exec(url);
          if (!results) return null;
          if (!results[2]) return '';
          return decodeURIComponent(results[2].replace(/\+/g, ' '));
        },

        async setStudentsPlan (uid) {
          secondaryFirebaseApp.default.database().ref("/teachers/" + uid + "/students").once('value')
          .then((snapshot) => {
            snapshot.forEach((student) => {
              var studentRef = firebase.default.database().ref("/students/" + student.val());
              studentRef.child('plan').set(this.userPlan);
            })
          })
        },
        async getStudentPlan (uid) {
          secondaryFirebaseApp.default.database().ref("/students/" + uid + "/teacherId").once('value')
          .then((snapshot) => {
            firebase.default.database().ref("/teachers/" + snapshot.val() + "/email").once('value')
            .then((childSnapshot) => {
              this.checkSubscription(childSnapshot.val());
            })
          })
        },
        async setUserLogged (user) {
          this.user = user;
          this.loggedIn = true;
          this.userType = "teacher";
          this.$router.replace({ name: "Home" });
        },
        async displayStudentsDetails (student) {
          this.student = student;
          this.$router.replace({ name: "studentDetails" });
        },
        async logout () {
          try {
            const data = firebase.default.app("secondaryFirebaseApp").auth().signOut();
            this.userType = '';
            this.error = "";
            this.loggedIn = false;
            this.signingIn = false;
            this.$router.replace({name: "login"})
          } catch(err) {
            console.log(err)
          }
        }
      },
      render: h => h(App)
    }).$mount('#app')
  }
})

new Vue({
  components: { Unity }
})